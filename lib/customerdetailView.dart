import 'package:flutter/material.dart';

class CustomerDetailView extends StatefulWidget {
  const CustomerDetailView({Key key}) : super(key: key);

  @override
  _CustomerDetailViewState createState() => _CustomerDetailViewState();
}

class _CustomerDetailViewState extends State<CustomerDetailView> {

var _direcciones  = [];


  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments as dynamic;
    setState(() {
    _direcciones = args["Direcciones"];
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('Detalle de cliente'),
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          
                crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              
              child: 
               _getDetail('Nombre: ', args["Nombre"]),
            ),
            _getDetail('Apellido: ', args["Apellido"]),
            Divider(height: 10, color: Colors.transparent,),
            Container(
              child: Text('Direcciones:', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            ),

            Expanded(
              child: Container(
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(_direcciones[index]["nombre"].toString()),
                      isThreeLine: true,
                      subtitle: Container(
                        child: Column(
                          children: [
                            _getDetail('Calle:', _direcciones[index]["Calle"].toString()),
                            _getDetail('numero:', _direcciones[index]["numero"].toString()),
                            _getDetail('sector:', _direcciones[index]["sector"].toString()),
                            _getDetail('telefono:', _direcciones[index]["telefono"].toString()),
                            _getDetail('contacto:', _direcciones[index]["contacto"].toString()),
                            Divider()
                          ],
                        ),
                      ),

                    );
                  },
                  itemCount: _direcciones.length,
                ),
              ),
            ),

            // "nombre":
            // "calle": 
            // "numero":
            // "sector":
            // "telefono
            // "contacto
            //
          ],
        ),
      ),
    );
  }

  Row _getDetail(String title, String value) {
    return Row(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  child: Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                ),
                Text(value,
                 style: TextStyle(
                      fontSize: 16,
                    ),),
              ],
            );
  }
}
