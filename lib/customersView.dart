import 'package:flutter/material.dart';

class CustomerView extends StatefulWidget {
  const CustomerView({Key key}) : super(key: key);

  @override
  _CustomerViewState createState() => _CustomerViewState();
}

class _CustomerViewState extends State<CustomerView> {
  var _clients = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      _clients = [
        {
          "Nombre": "Luis Gabriel",
          "Apellido": "Rosa Nuñez",
          "Direcciones": [
            {
              "nombre": "Mi primera dirección",
              "calle": "Duarte",
              "numero": "36",
              "sector": "Los Frailes",
              "telefono": "809-555-5555",
              "contacto": "Jose Perez"
            },
             {
              "nombre": "Mi segunda dirección",
              "calle": "Duarte",
              "numero": "36",
              "sector": "Los Frailes",
              "telefono": "809-555-5555",
              "contacto": "Jose Perez"
            },
             {
              "nombre": "Mi tercera dirección",
              "calle": "Duarte",
              "numero": "36",
              "sector": "Los Frailes",
              "telefono": "809-555-5555",
              "contacto": "Jose Perez"
            }
          ]
        },
        {
          "Nombre": "Crisostomo",
          "Apellido": "Castillo",
          "Direcciones": [{
            "nombre": "Mi primera dirección",
            "calle": "Duarte",
            "numero": "36",
            "sector": "Los Frailes",
            "telefono": "809-555-5555",
            "contacto": "Jose Perez"
          }],
        },
        {
          "Nombre": "Juan",
          "Apellido": "Duarte",
          "Direcciones": [{
            "nombre": "Mi primera dirección",
            "calle": "Duarte",
            "numero": "36",
            "sector": "Los Frailes",
            "telefono": "809-555-5555",
            "contacto": "Jose Perez"
          }],
        },
      ];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Clientes'),
      ),
      body: Container(
        child: ListView.builder(
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(_clients[index]["Nombre"] +
                  " " +
                  _clients[index]["Apellido"]),
              onTap: () {
                print('Pulsado');
                Navigator.pushNamed(context, '/CustomerDetail',
                    arguments: _clients[index]);
              },
            );
          },
          itemCount: _clients.length,
        ),
      ),
    );
  }
}
